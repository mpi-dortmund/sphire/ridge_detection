import unittest
from ridge_detection import convol
from numpy import array_equal
from pickle import load as pickle_load
from os import path

ABSOLUTE_PATH = path.dirname(path.realpath(__file__))


class Test_constant_values(unittest.TestCase):
    def test_constants(self):
        self.assertEqual(convol.SQRT_2_PI_INV, 0.398942280401432677939946059935)



class Test_phi_functions(unittest.TestCase):
    """ Since it just call the 'getNormal" function I do not test it deeply"""
    def test_phi0(self):
        self.assertEqual(convol.phi0(x=12, sigma=3), 0.9999683287581669)

    def test_phi1(self):
        self.assertEqual(convol.phi1(x=12, sigma=3), 4.461007525496179e-05)

    def test_phi2(self):
        self.assertEqual(convol.phi2(x=12, sigma=3), -5.948010033994906e-05)



class Test_compute_gauss_mask_functions(unittest.TestCase):
    """ Since it just call the 'getNormal" function I do not test it deeply"""
    def test__compute_gauss_mask_0(self):
        n,h= convol._compute_gauss_mask_0(sigma=3)
        self.assertEqual(n, 10)
        self.assertTrue(array_equal(h.flatten().tolist(),[0.0007709847844700191, 0.0015322813472258279, 0.003906399194080312, 0.008920474684459667, 0.018246367574581424, 0.033430693684040835, 0.054865303305523194, 0.08065587638926164, 0.10620915776234396, 0.12527862866310957, 0.13236766522180704, 0.12527862866310963, 0.10620915776234391, 0.08065587638926167, 0.05486530330552321, 0.03343069368404085, 0.018246367574581424, 0.008920474684459688, 0.00390639919408025, 0.0015322813472259096, 0.0007709847844699772]))

    def test__compute_gauss_mask_1(self):
        n,h= convol._compute_gauss_mask_1(sigma=3)
        self.assertEqual(n, 11)
        self.assertTrue(array_equal(h,[-0.00029089423168192005, -0.0005927644197947818, -0.0015183746033930394, -0.003440733576319773, -0.00687477433761648, -0.012052846684191708, -0.018402144035632884, -0.024160363296055712, -0.026637729952081232, -0.02338448378466565, -0.013791463112546795, 0.0, 0.013791463112546795, 0.02338448378466565, 0.026637729952081232, 0.024160363296055712, 0.018402144035632884, 0.012052846684191708, 0.00687477433761648, 0.003440733576319773, 0.0015183746033930394, 0.0005927644197947818, 0.00029089423168192005]))

    def test__compute_gauss_mask_2(self):
        n,h= convol._compute_gauss_mask_2(sigma=3)
        self.assertEqual(n, 12)
        self.assertTrue(array_equal(h,[0.00010949292900801937, 0.00022988367462088732, 0.0005933741951520563, 0.0013358361641515701, 0.0026003853963920613, 0.0043159184848130675, 0.005952568399360933, 0.006448806700816698, 0.004598748849729373, -8.206336716480231e-05, -0.006543766606641001, -0.012273264151684421, -0.014571841337108885, -0.012273264151684421, -0.006543766606641001, -8.206336716480231e-05, 0.004598748849729373, 0.006448806700816698, 0.005952568399360933, 0.0043159184848130675, 0.0026003853963920613, 0.0013358361641515701, 0.0005933741951520563, 0.00022988367462088732, 0.00010949292900801937]))



class Test_convolve_rows_gauss(unittest.TestCase):
    def test_convolve_rows_gauss(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_convolve_rows_gauss"), 'rb') as handle:
            image, mask, n,  h, width, height = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_convolve_rows_gauss"), 'rb') as handle:
            image_expected_out, mask_expected_out, n_expected_out, h_expected_out, width_expected_out, height_expected_out = pickle_load(handle)

        convol.convolve_rows_gauss(image=image, mask=mask, n=n, h=h, width=width, height=height)
        self.assertTrue(array_equal(image, image_expected_out))
        self.assertTrue(array_equal(h, h_expected_out))
        self.assertTrue(array_equal(mask, mask_expected_out))
        self.assertEqual(width, width_expected_out)
        self.assertEqual(height, height_expected_out)
        self.assertEqual(n, n_expected_out)



class Test_convolve_cols_gauss(unittest.TestCase):
    def test_convolve_cols_gauss(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_convolve_cols_gauss"), 'rb') as handle:
            h, mask,n, k, width, height = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_convolve_cols_gauss"), 'rb') as handle:
            h_expected_out, mask_expected_out, n_expected_out, k_expected_out, width_expected_out, height_expected_out = pickle_load(handle)

        convol.convolve_cols_gauss(h=h, mask=mask, n=n, k=k, width=width, height=height)
        self.assertTrue(array_equal(h, h_expected_out))
        self.assertTrue(array_equal(mask, mask_expected_out))
        self.assertTrue(array_equal(k, k_expected_out))
        self.assertEqual(width, width_expected_out)
        self.assertEqual(height, height_expected_out)
        self.assertEqual(n, n_expected_out)



class Test_convolve_gauss(unittest.TestCase):
    def test_convolve_gauss(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_convolveGauss"), 'rb') as handle:
            image, width, height, sigma = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_convolveGauss"), 'rb') as handle:
            image_expected_out, k_expected_out, width_expected_out, height_expected_out, sigma_expected_out = pickle_load(handle)
        k=convol.convolve_gauss(image=image,width=width, height=height, sigma=sigma)
        self.assertTrue(array_equal(image,image_expected_out))
        self.assertTrue(array_equal(k, k_expected_out))
        self.assertEqual(width,width_expected_out)
        self.assertEqual(height,height_expected_out)
        self.assertEqual(sigma, sigma_expected_out)

if __name__ == '__main__':
    unittest.main()
