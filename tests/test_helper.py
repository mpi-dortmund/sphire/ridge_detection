import unittest
from ridge_detection import helper
from os import path
from pickle import load as pickle_load
from numpy import array_equal,fromiter,ndarray,min as np_min,max as np_max

ABSOLUTE_PATH = path.dirname(path.realpath(__file__))


class Test_constant_values(unittest.TestCase):
    def test_constants(self):
        self.assertEqual(helper.SQRTPI, 1.772453850905516027)
        self.assertEqual(helper.SQRT2, 1.41421356237309504880)
        self.assertEqual(helper.UPPERLIMIT, 20.0)

        self.assertEqual(helper.P10, 242.66795523053175)
        self.assertEqual(helper.P11, 21.979261618294152)
        self.assertEqual(helper.P12, 6.9963834886191355)
        self.assertEqual(helper.P13, -.035609843701815385)
        self.assertEqual(helper.P20, 300.4592610201616005)
        self.assertEqual(helper.P21, 451.9189537118729422)
        self.assertEqual(helper.P22, 339.3208167343436870)
        self.assertEqual(helper.P23, 152.9892850469404039)
        self.assertEqual(helper.P24, 43.16222722205673530)
        self.assertEqual(helper.P25, 7.211758250883093659)
        self.assertEqual(helper.P26, .5641955174789739711)
        self.assertEqual(helper.P27, -.0000001368648573827167067)
        self.assertEqual(helper.P30, -.00299610707703542174)
        self.assertEqual(helper.P31, -.0494730910623250734)
        self.assertEqual(helper.P32, -.226956593539686930)
        self.assertEqual(helper.P33, -.278661308609647788)
        self.assertEqual(helper.P34, -.0223192459734184686)

        self.assertEqual(helper.Q10, 215.05887586986120)
        self.assertEqual(helper.Q11, 91.164905404514901)
        self.assertEqual(helper.Q12, 15.082797630407787)
        self.assertEqual(helper.Q13, 1.0)
        self.assertEqual(helper.Q20, 300.4592609569832933)
        self.assertEqual(helper.Q21, 790.9509253278980272)
        self.assertEqual(helper.Q22, 931.3540948506096211)
        self.assertEqual(helper.Q23, 638.9802644656311665)
        self.assertEqual(helper.Q24, 277.5854447439876434)
        self.assertEqual(helper.Q25, 77.00015293522947295)
        self.assertEqual(helper.Q26, 12.78272731962942351)
        self.assertEqual(helper.Q27, 1.0)
        self.assertEqual(helper.Q30, .0106209230528467918)
        self.assertEqual(helper.Q31, .191308926107829841)
        self.assertEqual(helper.Q32, 1.05167510706793207)
        self.assertEqual(helper.Q33, 1.98733201817135256)
        self.assertEqual(helper.Q34, 1.0)


#todo: is it possible replace with this function https://docs.scipy.org/doc/scipy-0.16.1/reference/generated/scipy.stats.norm.html?
class Test_getNormal(unittest.TestCase):
    def test_greater_than_UPPERLIMIT(self):
        self.assertEqual(helper.getNormal(x=helper.UPPERLIMIT + 1), 1)

    def test_smaller_than_negativeUPPERLIMIT(self):
        self.assertEqual(helper.getNormal(x=-helper.UPPERLIMIT - 1), 0)

    """positive y"""
    def test_y_pos_smaller04(self):
        self.assertEqual(helper.getNormal(x=0.1), 0.539827837277029)

    def test_y_pos_between04_4(self):
        self.assertEqual(helper.getNormal(x=3), 0.9986501019683699)

    def test_y_pos_bigger04(self):
        self.assertEqual(helper.getNormal(x=8), 0.9999999999999993)
    """negative x"""

    def test_y_neg_smaller04(self):
        self.assertEqual(helper.getNormal(x=-0.1), 0.46017216272297107)

    def test_y_neg_between04_4(self):
        self.assertEqual(helper.getNormal(x=-3), 0.0013498980316300959)

    def test_y_neg_bigger04(self):
        self.assertEqual(helper.getNormal(x=-8), 6.307451597500211e-16)



class Test_normalizeImg(unittest.TestCase):
    img = fromiter(range(250, 260), dtype="int").reshape(2, 5)
    def test_default_values(self):
        out=helper.normalizeImg(self.img)
        self.assertTrue(array_equal(out,[0.0, 28.333333333333332, 56.666666666666664, 85.0, 113.33333333333333, 141.66666666666669, 170.0, 198.33333333333334, 226.66666666666666, 255.0]))
        self.assertEqual(min(out),0)
        self.assertEqual(max(out), 255)

    def test_different_range(self):
        out=helper.normalizeImg(self.img,new_min=5,new_max=10)
        self.assertTrue(array_equal(out,[5.0, 5.555555555555555, 6.111111111111111, 6.666666666666666, 7.222222222222222, 7.777777777777778, 8.333333333333332, 8.88888888888889, 9.444444444444445, 10.0]))
        self.assertEqual(min(out),5)
        self.assertEqual(max(out), 10)

    def test_returns_np_array(self):
        out=helper.normalizeImg(self.img,return_Aslist=False)
        self.assertTrue(isinstance(out,ndarray))
        self.assertTrue(array_equal(out.flatten().tolist(),[0.0, 28.333333333333332, 56.666666666666664, 85.0, 113.33333333333333, 141.66666666666669, 170.0, 198.33333333333334, 226.66666666666666, 255.0]))
        self.assertEqual(np_min(out),0)
        self.assertEqual(np_max(out), 255)


class Test_threshold(unittest.TestCase):
    def test_threshold(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_threshold"), 'rb') as handle:
            image, minimum, width, height, region = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_threshold"), 'rb') as handle:
            image_expected_out, minimum_expected_out, width_expected_out, height_expected_out, region_expected_out = pickle_load(handle)

        helper.threshold(image=image, minimum=minimum, width=width, height=height, out_region=region)
        self.assertTrue(array_equal(image,image_expected_out))
        self.assertEqual(height,height_expected_out)
        self.assertEqual(width,width_expected_out)
        self.assertEqual(minimum,minimum_expected_out)
        self.assertEqual(region.num, region_expected_out.num)
        self.assertEqual(len(region._rl), len(region_expected_out._rl))
        for chord_i,chord_j in zip(region._rl,region_expected_out._rl):
            self.assertEqual(chord_i.cb,chord_j.cb)
            self.assertEqual(chord_i.ce, chord_j.ce)
            self.assertEqual(chord_i.r, chord_j.r)


if __name__ == '__main__':
    unittest.main()
