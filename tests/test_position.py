import unittest
from ridge_detection import position
from os import path
from numpy import array_equal
from pickle import load as pickle_load

ABSOLUTE_PATH = path.dirname(path.realpath(__file__))


class Test_constant_values(unittest.TestCase):
    def test_constants(self):
        self.assertEqual(position.PIXEL_BOUNDARY, 0.6)



class Test_solve_linear(unittest.TestCase):
    def test_a_is_0(self):
        v1,v2= position.solve_linear(a=0, b=10)
        self.assertEqual(v2,0)
        self.assertIsNone(v1)

    def test_a_not_0(self):
        v1,v2= position.solve_linear(a=3, b=10)
        self.assertEqual(v1,-3.3333333333333335)
        self.assertEqual(v2,1)



class Test_compute_eigenvals(unittest.TestCase):
    def test_compute_eigenvals(self):
        eigval_expected=[0, 0]
        eigvec_expected=[[0, 0], [0, 0]]
        dfdrr = 0.7301039421822881
        dfdrc = 9.906502151441844e-33
        dfdcc = 0.9906613629791292,
        position.compute_eigenvals(dfdrr=0.7301039421822881, dfdrc=9.906502151441844e-33, dfdcc=0.9906613629791292, eigval=eigval_expected, eigvec=eigvec_expected)
        self.assertEqual(dfdrr, 0.7301039421822881)
        self.assertEqual(dfdrc, 9.906502151441844e-33)
        self.assertEqual(dfdcc, (0.9906613629791292,))
        self.assertTrue(array_equal(eigval_expected, [0.9906613629791292, 0.7301039421822881]))
        self.assertTrue(array_equal(eigvec_expected,[[3.802041838281026e-32, 1.0], [1.0, -3.802041838281026e-32]]))



class Test_compute_line_points(unittest.TestCase):
    def test_compute_line_points(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_compute_line_points"), 'rb') as handle:
            k, ismax, ev, n1, n2, p1, p2, width, height, low, high, mode = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_compute_line_points"), 'rb') as handle:
            k_expected , ismax_expected , ev_expected , n1_expected , n2_expected , p1_expected , p2_expected , width_expected , height_expected , low_expected, high_expected , mode_expected = pickle_load(handle)

        position.compute_line_points(ku=k, ismax=ismax, ev=ev, nx=n1, ny=n2, px=p1, py=p2, width=width, height=height, low=low, high=high, mode=mode)

        self.assertTrue(array_equal(ismax, ismax_expected))
        self.assertTrue(array_equal(ev, ev_expected))
        self.assertTrue(array_equal(n1, n1_expected))
        self.assertTrue(array_equal(n2, n2_expected))
        self.assertTrue(array_equal(p1, p1_expected))
        self.assertTrue(array_equal(p2, p2_expected))

        self.assertEqual(height,height_expected)
        self.assertEqual(width,width_expected)
        self.assertEqual(high, high_expected)
        self.assertEqual(low, low_expected)
        self.assertEqual(mode, mode_expected)




class Test_detect_lines(unittest.TestCase):
    def test_detect_lines(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_detect_lines"), 'rb') as handle:
            image, width, height, contours,  sigma, low, high, mode, compute_width, correct_pos, extend_lines, junctions = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_detect_lines"), 'rb') as handle:
            image_expected , width_expected , height_expected , contours_expected ,  sigma_expected , low_expected , high_expected , mode_expected , compute_width_expected , correct_pos_expected , extend_lines_expected , junctions_expected ,num_result_expected = pickle_load(
                handle)
        num_result= position.detect_lines(image=image, width=width, height=height, contours=contours, sigma=sigma, low=low, high=high, mode=mode, compute_width=compute_width, correct_pos=correct_pos, extend_lines=extend_lines, junctions=junctions)

        self.assertEqual(num_result, num_result_expected)
        self.assertEqual(height, height_expected)
        self.assertEqual(width, width_expected)
        self.assertEqual(high, high_expected)
        self.assertEqual(low, low_expected)
        self.assertEqual(mode, mode_expected)
        self.assertEqual(sigma,sigma_expected)

        self.assertTrue(extend_lines==extend_lines_expected)
        self.assertTrue(compute_width == compute_width_expected)
        self.assertTrue(correct_pos == correct_pos_expected)

        self.assertEqual(len(junctions),len(junctions_expected))
        for j1,j2 in zip(junctions,junctions_expected):
            self.assertEqual(j1.cont1,j2.cont1)
            self.assertEqual(j1.cont2, j2.cont2)
            self.assertEqual(j1.pos, j2.pos)
            self.assertEqual(j1.y, j2.y)
            self.assertEqual(j1.x, j2.x)
            if j1.father_frame is not None:
                self.assertTrue(array_equal(j1.father_frame, j2.father_frame))
            self.assertTrue(j1.isNonTerminal==j2.isNonTerminal)
            # I skip the lineCont because they are None

        self.assertEqual(len(contours), len(contours_expected))
        for line_i, line_j in zip(contours, contours_expected):
            # self.assertEqual(line_i._id,line_j._id)        # since it is based on a global counter variable, when i run 'test_all.py' it fails
            self.assertEqual(line_i.num, line_j.num)
            if line_i.angle is not None:
                self.assertTrue(array_equal(line_i.angle, line_j.angle))
            if line_i.asymmetry is not None:
                self.assertTrue(array_equal(line_i.asymmetry, line_j.asymmetry))
            if line_i.col is not None:
                self.assertTrue(array_equal(line_i.col, line_j.col))
            if line_i.response is not None:
                self.assertTrue(array_equal(line_i.response, line_j.response))
            if line_i.row is not None:
                self.assertTrue(array_equal(line_i.row, line_j.row))
            if line_i.intensity is not None:
                self.assertTrue(array_equal(line_i.intensity, line_j.intensity))
            if line_i.width_l is not None:
                self.assertTrue(array_equal(line_i.width_l, line_j.width_l))
            if line_i.width_r is not None:
                self.assertTrue(array_equal(line_i.width_r, line_j.width_r))
            self.assertEqual(line_i.cont_class, line_j.cont_class)
            if line_i.father_frame is not None:
                self.assertTrue(array_equal(line_i.father_frame, line_j.father_frame))
            if line_i.frame is not None:
                self.assertTrue(array_equal(line_i.frame, line_j.frame))


if __name__ == '__main__':
    unittest.main()
