import unittest
from ridge_detection import linesUtil


class Test_constant_values(unittest.TestCase):
    def test_constants(self):
        self.assertEqual(linesUtil.DERIV_R, 1)
        self.assertEqual(linesUtil.DERIV_C, 2)
        self.assertEqual(linesUtil.DERIV_RR, 3)
        self.assertEqual(linesUtil.DERIV_RC, 4)
        self.assertEqual(linesUtil.DERIV_CC, 5)

        self.assertEqual(linesUtil.MODE_LIGHT, 1)
        self.assertEqual(linesUtil.MODE_DARK, 2)

        self.assertEqual(linesUtil.INITIAL_SIZE, 100)
        self.assertEqual(linesUtil.REALLOC_FACTOR, 2)

        self.assertEqual(linesUtil.MAX_SIZE_MASK_0, 3.09023230616781)
        self.assertEqual(linesUtil.MAX_SIZE_MASK_1, 3.46087178201605)
        self.assertEqual(linesUtil.MAX_SIZE_MASK_2, 3.82922419517181)

        self.assertEqual(linesUtil.ERR_SOR, "Sigma out of range:")

        self.assertEqual(linesUtil.COUNTOUR_DICT['cont_no_junc'], 0)
        self.assertEqual(linesUtil.COUNTOUR_DICT['cont_start_junc'], 1)
        self.assertEqual(linesUtil.COUNTOUR_DICT['cont_end_junc'], 2)
        self.assertEqual(linesUtil.COUNTOUR_DICT['cont_both_junc'], 3)



class Test_mask_size(unittest.TestCase):
    def test_mask_size(self):
        self.assertEqual(linesUtil.mask_size(maximum=2.01, sigma=3.1), 7)



class Test_lincoord(unittest.TestCase):
    def test_lincoord(self):
        self.assertEqual(linesUtil.lincoord(row=31, col=20, width=3), 113)



class Test_br(unittest.TestCase):
    def test_negative_row(self):
        self.assertEqual(linesUtil.br(row= -3, height=10), 3)

    def test_notBigger_height(self):
        self.assertEqual(linesUtil.br(row= 3, height=10), 3)

    def test_Bigger_height(self):
        self.assertEqual(linesUtil.br(row= 30, height=10), -12)


class Test_col(unittest.TestCase):
    def test_negative_col(self):
        self.assertEqual(linesUtil.bc(col= -3, width=10), 3)

    def test_notBigger_width(self):
        self.assertEqual(linesUtil.bc(col= 3, width=10), 3)

    def test_Bigger_width(self):
        self.assertEqual(linesUtil.bc(col= 30, width=10), -12)




if __name__ == '__main__':
    unittest.main()
