import unittest
from ridge_detection import params

JSON_FILE ="../example_config.json"

class Test_Params_class(unittest.TestCase):
    p = params.Params(JSON_FILE)

    def test_loadJson(self):
        json= params.load_json(JSON_FILE)
        expected_json={'path_to_file': '../example.tif',
         'mandatory_parameters': {'Sigma': 3.39, 'Lower_Threshold': 0.34, 'Upper_Threshold': 1.02,
                                  'Maximum_Line_Length': 0, 'Minimum_Line_Length': 0, 'Darkline': 'LIGHT',
                                  'Overlap_resolution': 'NONE'},
         'optional_parameters': {'Line_width': 10.0, 'High_contrast': 200, 'Low_contrast': 80},
         'further_options': {'Correct_position': True, 'Estimate_width': True, 'doExtendLine': True,
                             'Show_junction_points': True, 'Show_IDs': False, 'Display_results': True, 'Preview': True,
                             'Make_Binary': False, 'save_on_disk': True}}
        mandatory=json["mandatory_parameters"]
        optional = json["optional_parameters"]
        further = json["further_options"]

        self.assertDictEqual(json,expected_json)
        self.assertEqual(mandatory["Sigma"], 3.39 )
        self.assertEqual(mandatory["Lower_Threshold"], 0.34)
        self.assertEqual(mandatory["Upper_Threshold"], 1.02)
        self.assertEqual(mandatory["Maximum_Line_Length"], 0)
        self.assertEqual(mandatory["Minimum_Line_Length"], 0)
        self.assertEqual(mandatory["Darkline"], "LIGHT")
        self.assertEqual(mandatory["Overlap_resolution"], "NONE")

        self.assertEqual(optional["Line_width"], 10.0 )
        self.assertEqual(optional["High_contrast"], 200)
        self.assertEqual(optional["Low_contrast"], 80)

        self.assertTrue(further["Correct_position"])
        self.assertTrue(further["Estimate_width"])
        self.assertTrue(further["doExtendLine"])
        self.assertTrue(further["Show_junction_points"])
        self.assertTrue(further["Display_results"])
        self.assertTrue(further["Preview"])
        self.assertFalse(further["Show_IDs"])
        self.assertTrue(further["save_on_disk"])
        self.assertFalse(further["Make_Binary"])

    def test_params_class_values(self):
        self.assertEqual(self.p.get_sigma(), 3.39)
        self.assertEqual(self.p.get_lower_Threshold(), 0.34)
        self.assertEqual(self.p.get_upper_Threshold(), 1.02)
        self.assertEqual(self.p.get_maximum_Line_Length(), 0)
        self.assertEqual(self.p.get_minimum_Line_Length(), 0)
        self.assertEqual(self.p.get_darkline(), 1)
        self.assertEqual(self.p.get_overlap_resolution(), "none")

        self.assertEqual(self.p.get_line_width(), 10.0)
        self.assertEqual(self.p.get_high_contrast(), 200)
        self.assertEqual(self.p.get_low_contrast(), 80)

        self.assertTrue(self.p.get_correct_position())
        self.assertTrue(self.p.get_estimate_width())
        self.assertTrue(self.p.get_doExtendLine())
        self.assertTrue(self.p.get_show_junction_points())
        self.assertTrue(self.p.get_display_results())
        self.assertTrue(self.p.get_preview())
        self.assertFalse(self.p.get_show_IDs())

    def test_isValid(self):
        self.assertTrue(self.p._isValid() is None)

    def test__get_mandatory_parameters(self):
        a=self.p._get_mandatory_parameters()
        for k in self.p._config["mandatory_parameters"].keys():
            self.assertTrue(k in a)

    def test__get_optional_parameters(self):
        a=self.p._get_optional_parameters()
        for k in self.p._config["optional_parameters"].keys():
            self.assertTrue(k in a)

    def test__get_further_options(self):
        a=self.p._get_further_options()
        for k in self.p._config["further_options"].keys():
            self.assertTrue(k in a)


if __name__ == '__main__':
    unittest.main()
