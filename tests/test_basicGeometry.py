import unittest
from ridge_detection import basicGeometry


class Test_id_counter(unittest.TestCase):
    def test_not_init_id(self):
        basicGeometry.reset_counter()
        self.assertEqual(basicGeometry.id_counter, 0)

    def test_add_id(self):
        basicGeometry.reset_counter()
        basicGeometry.id_counter+=1
        self.assertEqual(basicGeometry.id_counter, 1)

    def test_resetcounter(self):
        basicGeometry.id_counter += 1
        self.assertNotEqual(basicGeometry.id_counter, 0)
        basicGeometry.reset_counter()
        self.assertEqual(basicGeometry.id_counter, 0)



class Test_getIndexByID(unittest.TestCase):
    def test_not_found(self):
        self.assertEqual(basicGeometry.getIndexByID([], 0), -1)

    def test_found(self):
        l=[basicGeometry.Line(), basicGeometry.Line(), basicGeometry.Line()]
        self.assertEqual(basicGeometry.getIndexByID(l, 1), 1)



class Test_getStartOrdEndPosition(unittest.TestCase):
    def test_getStartOrdEndPosition_start_NOTsmaller_than_end(self):
        line = basicGeometry.Line(num=10, col=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], row=[10, 10, 20, 30, 40, 50, 60, 70, 80, 90])
        self.assertEqual(line.getStartOrdEndPosition(5,5),0)

    def test_getStartOrdEndPosition_start_smaller_than_en(self):
        line = basicGeometry.Line(num=10, col=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], row=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        self.assertEqual(line.getStartOrdEndPosition(5,4),line.num-1)



class Test_estimateLength(unittest.TestCase):
    def test_getStartOrdEndPosition_start_NOTsmaller_than_end(self):
        line = basicGeometry.Line(num=10, col=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], row=[10, 10, 20, 30, 40, 50, 60, 70, 80, 90])
        self.assertEqual(line.estimateLength(),161.90366321928204)



if __name__ == '__main__':
    unittest.main()
