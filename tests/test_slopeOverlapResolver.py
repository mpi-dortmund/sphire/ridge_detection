import unittest
from ridge_detection import slopeOverlapResolver
from pickle import load as pickle_load
from os import path
ABSOLUTE_PATH = path.dirname(path.realpath(__file__))
from numpy import array_equal
from operator import attrgetter
from copy import deepcopy


class Test_constant_values(unittest.TestCase):
    def test_constants(self):
        self.assertEqual(slopeOverlapResolver.FLOAT_MAX_VALUE_JAVA, 3.4028235E38)
        self.assertEqual(slopeOverlapResolver.SIGMA, 2.0)
        self.assertEqual(slopeOverlapResolver.SLOPE_DIST, 5)
        self.assertEqual(slopeOverlapResolver.STRAIGHT_TOLERANCE, 1.02)


class Test_straightCalc(unittest.TestCase):
    def test_constants(self):
        self.assertEqual(
            slopeOverlapResolver.straightCalc(points=[[0, 11], [12, 32], [4, 2], [21, 1], [22, 3], [8, 6], [5, 3]]), 9.864450376630154)



class Test_dist(unittest.TestCase):
    def test_constants(self):
        self.assertEqual(slopeOverlapResolver.dist(p1=[19, 32], p2=[2, 2]), 34.48187929913333)



#todo: Since after sorting contours_expected and contours we still have a different order of their elements this test can failed
class Test_resolve(unittest.TestCase):
    def test_resolve(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_resolve"), 'rb') as handle:
            contours, resultJunction = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_resolve"), 'rb') as handle:
            contours_expected, resultJunction_expected = pickle_load(handle)
        contours = slopeOverlapResolver.resolve(originalLines=contours, junctions=resultJunction)
        contours=list(contours)
        contours_expected = list(contours_expected)
        contours.sort(key=attrgetter('_id','num','cont_class'))
        contours_expected.sort(key=attrgetter('_id','num','cont_class'))

        for cont, cont_expected in zip(contours, contours_expected):
#            self.assertEqual(cont.cont_class, cont_expected.cont_class)  # it fails becuase the global var used in the code
            self.assertEqual(cont.num, cont_expected.num)                  
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))


        for j, j_expected in zip(resultJunction, resultJunction_expected):
            self.assertEqual(j.cont1, j_expected.cont1)
            self.assertEqual(j.cont2, j_expected.cont2)
            self.assertEqual(j.x, j_expected.x)
            self.assertEqual(j.y, j_expected.y)
            self.assertEqual(j.pos, j_expected.pos)

            self.assertTrue(j.father_frame == j_expected.father_frame)
            self.assertTrue(j.isNonTerminal == j_expected.isNonTerminal)

            self.assertEqual(j.lineCont1.cont_class, j_expected.lineCont1.cont_class)
            self.assertEqual(j.lineCont1.num, j_expected.lineCont1.num)
            self.assertTrue(array_equal(j.lineCont1.angle, j_expected.lineCont1.angle))
            self.assertTrue(array_equal(j.lineCont1.col, j_expected.lineCont1.col))
            self.assertTrue(array_equal(j.lineCont1.response, j_expected.lineCont1.response))
            self.assertTrue(array_equal(j.lineCont1.row, j_expected.lineCont1.row))
            self.assertTrue(j.lineCont1.father_frame == j_expected.lineCont1.father_frame)
            self.assertTrue(j.lineCont1.frame == j_expected.lineCont1.frame)
            self.assertTrue(array_equal(j.lineCont1.width_r, j_expected.lineCont1.width_r))
            self.assertTrue(array_equal(j.lineCont1.width_l, j_expected.lineCont1.width_l))
            self.assertTrue(array_equal(j.lineCont1.intensity, j_expected.lineCont1.intensity))

            self.assertEqual(j.lineCont2.cont_class, j_expected.lineCont2.cont_class)
            self.assertEqual(j.lineCont2.num, j_expected.lineCont2.num)
            self.assertTrue(array_equal(j.lineCont2.angle, j_expected.lineCont2.angle))
            self.assertTrue(array_equal(j.lineCont2.col, j_expected.lineCont2.col))
            self.assertTrue(array_equal(j.lineCont2.response, j_expected.lineCont2.response))
            self.assertTrue(array_equal(j.lineCont2.row, j_expected.lineCont2.row))
            self.assertTrue(j.lineCont2.father_frame == j_expected.lineCont2.father_frame)
            self.assertTrue(j.lineCont2.frame == j_expected.lineCont2.frame)
            self.assertTrue(array_equal(j.lineCont2.width_r, j_expected.lineCont2.width_r))
            self.assertTrue(array_equal(j.lineCont2.width_l, j_expected.lineCont2.width_l))
            self.assertTrue(array_equal(j.lineCont2.intensity, j_expected.lineCont2.intensity))


class Test_findOverlap(unittest.TestCase):
    def test_findoverlap(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_findOverlap"), 'rb') as handle:
            enclosedLines, nWayIntersections, junctions = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_findOverlap"), 'rb') as handle:
            enclosedLines_expected, nWayIntersections_expected, junctions_expected = pickle_load(handle)
        slopeOverlapResolver.findOverlap(enclosed=enclosedLines, nway=nWayIntersections, junctions=junctions)


        enclosedLines = list(enclosedLines)
        enclosedLines_expected = list(enclosedLines_expected)
        enclosedLines.sort(key=attrgetter('_id', 'num'))
        enclosedLines_expected.sort(key=attrgetter('_id', 'num'))

        for cont, cont_expected in zip(enclosedLines, enclosedLines_expected):
            self.assertEqual(cont.cont_class, cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))

        for j, j_expected in zip(junctions, junctions_expected):
            self.assertEqual(j.cont1, j_expected.cont1)
            self.assertEqual(j.cont2, j_expected.cont2)
            self.assertEqual(j.x, j_expected.x)
            self.assertEqual(j.y, j_expected.y)
            self.assertEqual(j.pos, j_expected.pos)

            self.assertTrue(j.father_frame == j_expected.father_frame)
            self.assertTrue(j.isNonTerminal == j_expected.isNonTerminal)

            self.assertEqual(j.lineCont1.cont_class, j_expected.lineCont1.cont_class)
            self.assertEqual(j.lineCont1.num, j_expected.lineCont1.num)
            self.assertTrue(array_equal(j.lineCont1.angle, j_expected.lineCont1.angle))
            self.assertTrue(array_equal(j.lineCont1.col, j_expected.lineCont1.col))
            self.assertTrue(array_equal(j.lineCont1.response, j_expected.lineCont1.response))
            self.assertTrue(array_equal(j.lineCont1.row, j_expected.lineCont1.row))
            self.assertTrue(j.lineCont1.father_frame == j_expected.lineCont1.father_frame)
            self.assertTrue(j.lineCont1.frame == j_expected.lineCont1.frame)
            self.assertTrue(array_equal(j.lineCont1.width_r, j_expected.lineCont1.width_r))
            self.assertTrue(array_equal(j.lineCont1.width_l, j_expected.lineCont1.width_l))
            self.assertTrue(array_equal(j.lineCont1.intensity, j_expected.lineCont1.intensity))

            self.assertEqual(j.lineCont2.cont_class, j_expected.lineCont2.cont_class)
            self.assertEqual(j.lineCont2.num, j_expected.lineCont2.num)
            self.assertTrue(array_equal(j.lineCont2.angle, j_expected.lineCont2.angle))
            self.assertTrue(array_equal(j.lineCont2.col, j_expected.lineCont2.col))
            self.assertTrue(array_equal(j.lineCont2.response, j_expected.lineCont2.response))
            self.assertTrue(array_equal(j.lineCont2.row, j_expected.lineCont2.row))
            self.assertTrue(j.lineCont2.father_frame == j_expected.lineCont2.father_frame)
            self.assertTrue(j.lineCont2.frame == j_expected.lineCont2.frame)
            self.assertTrue(array_equal(j.lineCont2.width_r, j_expected.lineCont2.width_r))
            self.assertTrue(array_equal(j.lineCont2.width_l, j_expected.lineCont2.width_l))
            self.assertTrue(array_equal(j.lineCont2.intensity, j_expected.lineCont2.intensity))

        self.assertTrue(nWayIntersections==nWayIntersections_expected)


class Test_buildIntersectionMaps(unittest.TestCase):
    def test_buildIntersectionMaps(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_buildIntersectionMaps"), 'rb') as handle:
            originalLines, enclosedLines, startIntersections, endIntersections = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_buildIntersectionMaps"), 'rb') as handle:
            originalLines_expected , enclosedLines_expected , startIntersections_expected, endIntersections_expected = pickle_load(handle)
        slopeOverlapResolver.buildIntersectionMaps(lines=originalLines, enclosedLines=enclosedLines, startIntersections=startIntersections, endIntersections=endIntersections)

        enclosedLines = list(enclosedLines)
        enclosedLines_expected = list(enclosedLines_expected)
        enclosedLines.sort(key=attrgetter('_id', 'num'))
        enclosedLines_expected.sort(key=attrgetter('_id', 'num'))

        for cont, cont_expected in zip(enclosedLines, enclosedLines_expected):
            self.assertEqual(cont.cont_class, cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))


        originalLines = list(originalLines)
        originalLines_expected = list(originalLines_expected)
        originalLines.sort(key=attrgetter('_id', 'num'))
        originalLines_expected.sort(key=attrgetter('_id', 'num'))

        for cont, cont_expected in zip(originalLines, originalLines_expected):
            self.assertEqual(cont.cont_class, cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))

        l_startIntersections=[ value for key, value in startIntersections.items()]
        l_startIntersections_expected = [value for key, value in startIntersections_expected.items()]
        l_startIntersections.sort(key=attrgetter('_id', 'num'))
        l_startIntersections_expected.sort(key=attrgetter('_id', 'num'))

        for cont, cont_expected in zip(l_startIntersections, l_startIntersections_expected):
            self.assertEqual(cont.cont_class, cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))

        l_endIntersections=[ value for key, value in endIntersections.items()]
        l_endIntersections_expected = [value for key, value in endIntersections_expected.items()]
        l_endIntersections.sort(key=attrgetter('_id', 'num'))
        l_endIntersections_expected.sort(key=attrgetter('_id', 'num'))

        for cont, cont_expected in zip(l_endIntersections, l_endIntersections_expected):
            self.assertEqual(cont.cont_class, cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))



class Test_buildMergeList_A(unittest.TestCase):
    def test_buildMergeList_A(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_buildMergeList_A"), 'rb') as handle:
            lineMerges, enclosedLines, startIntersections, endIntersections = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_buildMergeList_A"), 'rb') as handle:
            lineMerges_expected, enclosedLines_expected, startIntersections_expected, endIntersections_expected = pickle_load(
                handle)
        slopeOverlapResolver.buildMergeList_A(lineMerges=lineMerges, enclosedLines=enclosedLines, startIntersections=startIntersections, endIntersections=endIntersections)

        enclosedLines = list(enclosedLines)
        enclosedLines_expected = list(enclosedLines_expected)
        enclosedLines.sort(key=attrgetter('_id', 'num'))
        enclosedLines_expected.sort(key=attrgetter('_id', 'num'))

        for cont, cont_expected in zip(enclosedLines, enclosedLines_expected):
            self.assertEqual(cont.cont_class, cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))

        l_startIntersections=[ value for key, value in startIntersections.items()]
        l_startIntersections_expected = [value for key, value in startIntersections_expected.items()]
        l_startIntersections.sort(key=attrgetter('_id', 'num'))
        l_startIntersections_expected.sort(key=attrgetter('_id', 'num'))

        for cont, cont_expected in zip(l_startIntersections, l_startIntersections_expected):
            self.assertEqual(cont.cont_class, cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))

        l_endIntersections=[ value for key, value in endIntersections.items()]
        l_endIntersections_expected = [value for key, value in endIntersections_expected.items()]
        l_endIntersections.sort(key=attrgetter('_id', 'num'))
        l_endIntersections_expected.sort(key=attrgetter('_id', 'num'))

        for cont, cont_expected in zip(l_endIntersections, l_endIntersections_expected):
            self.assertEqual(cont.cont_class, cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))

        originalLines = [v[0] for v in lineMerges]
        originalLines_expected = [v[0] for v in lineMerges_expected]
        originalLines.sort(key=attrgetter('_id', 'num'))
        originalLines_expected.sort(key=attrgetter('_id', 'num'))

        """
        for cont, cont_expected in zip(originalLines, originalLines_expected):
            self.assertEqual(cont.cont_class, cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))
        """

class Test_buildMergeList_B(unittest.TestCase):
    def test_buildMergeList_B(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_buildMergeList_B"), 'rb') as handle:
            lineMerges, nWayIntersections = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_buildMergeList_B"), 'rb') as handle:
            lineMerges_expected, nWayIntersections_expected = pickle_load(
                handle)
        slopeOverlapResolver.buildMergeList_B(lineMerges=lineMerges, nWayIntersections=nWayIntersections)

        originalLines = [v[0] for v in lineMerges]
        originalLines_expected = [v[0] for v in lineMerges_expected]
        originalLines.sort(key=attrgetter('_id', 'num'))
        originalLines_expected.sort(key=attrgetter('_id', 'num'))

        for cont, cont_expected in zip(originalLines, originalLines_expected):
            self.assertEqual(cont.cont_class, cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))
        self.assertTrue(nWayIntersections == nWayIntersections_expected)


class Test_buildResolvedList(unittest.TestCase):

    def test_buildResolvedList(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_buildResolvedList"), 'rb') as handle:
            originalLines, lineMerges, lineMap = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_buildResolvedList"), 'rb') as handle:
            originalLines_expected, lineMerges_expected, lineMap_expected, resolvedLines_expected = pickle_load(handle)
        resolvedLines= slopeOverlapResolver.buildResolvedList(originalLines=originalLines, lineMerges=lineMerges, lineMap=lineMap)

        self.assertTrue(lineMap == lineMap_expected)

        lineMerges = [v[0] for v in lineMerges]
        lineMerges_expected = [v[0] for v in lineMerges_expected]
        lineMerges.sort(key=attrgetter('_id', 'num'))
        lineMerges_expected.sort(key=attrgetter('_id', 'num'))

        for cont, cont_expected in zip(lineMerges, lineMerges_expected):
            self.assertEqual(cont.cont_class, cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))

        for cont, cont_expected in zip(originalLines, originalLines_expected):
            self.assertEqual(cont.cont_class, cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))

        enclosedLines = list(resolvedLines)
        enclosedLines_expected = list(resolvedLines_expected)
        enclosedLines.sort(key=attrgetter('_id', 'num'))
        enclosedLines_expected.sort(key=attrgetter('_id', 'num'))

        for cont, cont_expected in zip(enclosedLines, enclosedLines_expected):
            #self.assertEqual(cont.cont_class, cont_expected.cont_class) #todo: it fails if i test the whole file because of the global var used in the code to count the lines
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame == cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))




class Test_updateJunctions(unittest.TestCase):
    def test_updatedJunctions(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_updatedJunctions"), 'rb') as handle:
            junctions, lineMap= pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_updatedJunctions"), 'rb') as handle:
            junctions_expected, lineMap_expected = pickle_load(handle)
        junctions = slopeOverlapResolver.updateJunctions(junctions=junctions, lineMap=lineMap)

        self.assertTrue(lineMap == lineMap_expected)
        for j, j_expected in zip(junctions, junctions_expected):
            self.assertEqual(j.cont1, j_expected.cont1)
            self.assertEqual(j.cont2, j_expected.cont2)
            self.assertEqual(j.x, j_expected.x)
            self.assertEqual(j.y, j_expected.y)
            self.assertEqual(j.pos, j_expected.pos)

            self.assertTrue(j.father_frame == j_expected.father_frame)
            self.assertTrue(j.isNonTerminal == j_expected.isNonTerminal)

            self.assertEqual(j.lineCont1.cont_class, j_expected.lineCont1.cont_class)
            self.assertEqual(j.lineCont1.num, j_expected.lineCont1.num)
            self.assertTrue(array_equal(j.lineCont1.angle, j_expected.lineCont1.angle))
            self.assertTrue(array_equal(j.lineCont1.col, j_expected.lineCont1.col))
            self.assertTrue(array_equal(j.lineCont1.response, j_expected.lineCont1.response))
            self.assertTrue(array_equal(j.lineCont1.row, j_expected.lineCont1.row))
            self.assertTrue(j.lineCont1.father_frame == j_expected.lineCont1.father_frame)
            self.assertTrue(j.lineCont1.frame == j_expected.lineCont1.frame)
            self.assertTrue(array_equal(j.lineCont1.width_r, j_expected.lineCont1.width_r))
            self.assertTrue(array_equal(j.lineCont1.width_l, j_expected.lineCont1.width_l))
            self.assertTrue(array_equal(j.lineCont1.intensity, j_expected.lineCont1.intensity))

            self.assertEqual(j.lineCont2.cont_class, j_expected.lineCont2.cont_class)
            self.assertEqual(j.lineCont2.num, j_expected.lineCont2.num)
            self.assertTrue(array_equal(j.lineCont2.angle, j_expected.lineCont2.angle))
            self.assertTrue(array_equal(j.lineCont2.col, j_expected.lineCont2.col))
            self.assertTrue(array_equal(j.lineCont2.response, j_expected.lineCont2.response))
            self.assertTrue(array_equal(j.lineCont2.row, j_expected.lineCont2.row))
            self.assertTrue(j.lineCont2.father_frame == j_expected.lineCont2.father_frame)
            self.assertTrue(j.lineCont2.frame == j_expected.lineCont2.frame)
            self.assertTrue(array_equal(j.lineCont2.width_r, j_expected.lineCont2.width_r))
            self.assertTrue(array_equal(j.lineCont2.width_l, j_expected.lineCont2.width_l))
            self.assertTrue(array_equal(j.lineCont2.intensity, j_expected.lineCont2.intensity))


class Test_pruneJunctions(unittest.TestCase):
    def test_pruneJunctions(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_pruneJunctions"), 'rb') as handle:
            junctions, updatedJunctions = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_pruneJunctions"), 'rb') as handle:
            junctions_expected, updatedJunctions_expected = pickle_load(handle)
        slopeOverlapResolver.pruneJunctions(junctions=junctions, updatedJunctions=updatedJunctions)

        self.assertTrue(updatedJunctions == updatedJunctions_expected)
        for j, j_expected in zip(junctions, junctions_expected):
            self.assertEqual(j.cont1, j_expected.cont1)
            self.assertEqual(j.cont2, j_expected.cont2)
            self.assertEqual(j.x, j_expected.x)
            self.assertEqual(j.y, j_expected.y)
            self.assertEqual(j.pos, j_expected.pos)

            self.assertTrue(j.father_frame == j_expected.father_frame)
            self.assertTrue(j.isNonTerminal == j_expected.isNonTerminal)

            self.assertEqual(j.lineCont1.cont_class, j_expected.lineCont1.cont_class)
            self.assertEqual(j.lineCont1.num, j_expected.lineCont1.num)
            self.assertTrue(array_equal(j.lineCont1.angle, j_expected.lineCont1.angle))
            self.assertTrue(array_equal(j.lineCont1.col, j_expected.lineCont1.col))
            self.assertTrue(array_equal(j.lineCont1.response, j_expected.lineCont1.response))
            self.assertTrue(array_equal(j.lineCont1.row, j_expected.lineCont1.row))
            self.assertTrue(j.lineCont1.father_frame == j_expected.lineCont1.father_frame)
            self.assertTrue(j.lineCont1.frame == j_expected.lineCont1.frame)
            self.assertTrue(array_equal(j.lineCont1.width_r, j_expected.lineCont1.width_r))
            self.assertTrue(array_equal(j.lineCont1.width_l, j_expected.lineCont1.width_l))
            self.assertTrue(array_equal(j.lineCont1.intensity, j_expected.lineCont1.intensity))

            self.assertEqual(j.lineCont2.cont_class, j_expected.lineCont2.cont_class)
            self.assertEqual(j.lineCont2.num, j_expected.lineCont2.num)
            self.assertTrue(array_equal(j.lineCont2.angle, j_expected.lineCont2.angle))
            self.assertTrue(array_equal(j.lineCont2.col, j_expected.lineCont2.col))
            self.assertTrue(array_equal(j.lineCont2.response, j_expected.lineCont2.response))
            self.assertTrue(array_equal(j.lineCont2.row, j_expected.lineCont2.row))
            self.assertTrue(j.lineCont2.father_frame == j_expected.lineCont2.father_frame)
            self.assertTrue(j.lineCont2.frame == j_expected.lineCont2.frame)
            self.assertTrue(array_equal(j.lineCont2.width_r, j_expected.lineCont2.width_r))
            self.assertTrue(array_equal(j.lineCont2.width_l, j_expected.lineCont2.width_l))
            self.assertTrue(array_equal(j.lineCont2.intensity, j_expected.lineCont2.intensity))


@unittest.skip(" I do not have a real case")
class Test_updateContourClasses(unittest.TestCase):
    pass



class Test_processLine(unittest.TestCase):
    def test_processLine_update_pos(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_processLine"), 'rb') as handle:
            j, cont,updatePos = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_processLine"), 'rb') as handle:
            j = pickle_load(handle)
        cont_expected = deepcopy(cont)
        j_expected = deepcopy(j)
        self.assertFalse(slopeOverlapResolver.processLine(j=j, line=cont, updatePos=updatePos))

        self.assertEqual(j.cont1, j_expected.cont1)
        self.assertEqual(j.cont2, j_expected.cont2)
        self.assertEqual(j.x, j_expected.x)
        self.assertEqual(j.y, j_expected.y)
        self.assertEqual(j.pos, j_expected.pos)

        self.assertTrue(j.father_frame == j_expected.father_frame)
        self.assertTrue(j.isNonTerminal == j_expected.isNonTerminal)

        self.assertEqual(j.lineCont1.cont_class, j_expected.lineCont1.cont_class)
        self.assertEqual(j.lineCont1.num, j_expected.lineCont1.num)
        self.assertTrue(array_equal(j.lineCont1.angle, j_expected.lineCont1.angle))
        self.assertTrue(array_equal(j.lineCont1.col, j_expected.lineCont1.col))
        self.assertTrue(array_equal(j.lineCont1.response, j_expected.lineCont1.response))
        self.assertTrue(array_equal(j.lineCont1.row, j_expected.lineCont1.row))
        self.assertTrue(j.lineCont1.father_frame == j_expected.lineCont1.father_frame)
        self.assertTrue(j.lineCont1.frame == j_expected.lineCont1.frame)
        self.assertTrue(array_equal(j.lineCont1.width_r, j_expected.lineCont1.width_r))
        self.assertTrue(array_equal(j.lineCont1.width_l, j_expected.lineCont1.width_l))
        self.assertTrue(array_equal(j.lineCont1.intensity, j_expected.lineCont1.intensity))

        self.assertEqual(j.lineCont2.cont_class, j_expected.lineCont2.cont_class)
        self.assertEqual(j.lineCont2.num, j_expected.lineCont2.num)
        self.assertTrue(array_equal(j.lineCont2.angle, j_expected.lineCont2.angle))
        self.assertTrue(array_equal(j.lineCont2.col, j_expected.lineCont2.col))
        self.assertTrue(array_equal(j.lineCont2.response, j_expected.lineCont2.response))
        self.assertTrue(array_equal(j.lineCont2.row, j_expected.lineCont2.row))
        self.assertTrue(j.lineCont2.father_frame == j_expected.lineCont2.father_frame)
        self.assertTrue(j.lineCont2.frame == j_expected.lineCont2.frame)
        self.assertTrue(array_equal(j.lineCont2.width_r, j_expected.lineCont2.width_r))
        self.assertTrue(array_equal(j.lineCont2.width_l, j_expected.lineCont2.width_l))
        self.assertTrue(array_equal(j.lineCont2.intensity, j_expected.lineCont2.intensity))

        #self.assertEqual(cont.cont_class, cont_expected.cont_class) #todo: it fails if i test the whole file because of the global var used in the code to count the lines
        self.assertEqual(cont.num, cont_expected.num)
        self.assertTrue(array_equal(cont.angle, cont_expected.angle))
        self.assertTrue(array_equal(cont.col, cont_expected.col))
        self.assertTrue(array_equal(cont.response, cont_expected.response))
        self.assertTrue(array_equal(cont.row, cont_expected.row))
        self.assertTrue(cont.father_frame == cont_expected.father_frame)
        self.assertTrue(cont.frame == cont_expected.frame)
        self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
        self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
        self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))

    def test_processLine_Not_update_pos(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_line"), 'rb') as handle:
            cont = pickle_load(handle)[0]
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_junc"), 'rb') as handle:
            j = pickle_load(handle)[0]
        cont_expected = deepcopy(cont)
        j_expected = deepcopy(j)
        self.assertFalse(slopeOverlapResolver.processLine(j=j, line=cont, updatePos=False))

        self.assertEqual(j.cont1, j_expected.cont1)
        self.assertEqual(j.cont2, j_expected.cont2)
        self.assertEqual(j.x, j_expected.x)
        self.assertEqual(j.y, j_expected.y)
        self.assertEqual(j.pos, j_expected.pos)

        self.assertTrue(j.father_frame == j_expected.father_frame)
        self.assertTrue(j.isNonTerminal == j_expected.isNonTerminal)

        self.assertEqual(j.lineCont1.cont_class, j_expected.lineCont1.cont_class)
        self.assertEqual(j.lineCont1.num, j_expected.lineCont1.num)
        self.assertTrue(array_equal(j.lineCont1.angle, j_expected.lineCont1.angle))
        self.assertTrue(array_equal(j.lineCont1.col, j_expected.lineCont1.col))
        self.assertTrue(array_equal(j.lineCont1.response, j_expected.lineCont1.response))
        self.assertTrue(array_equal(j.lineCont1.row, j_expected.lineCont1.row))
        self.assertTrue(j.lineCont1.father_frame == j_expected.lineCont1.father_frame)
        self.assertTrue(j.lineCont1.frame == j_expected.lineCont1.frame)
        self.assertTrue(array_equal(j.lineCont1.width_r, j_expected.lineCont1.width_r))
        self.assertTrue(array_equal(j.lineCont1.width_l, j_expected.lineCont1.width_l))
        self.assertTrue(array_equal(j.lineCont1.intensity, j_expected.lineCont1.intensity))

        self.assertEqual(j.lineCont2.cont_class, j_expected.lineCont2.cont_class)
        self.assertEqual(j.lineCont2.num, j_expected.lineCont2.num)
        self.assertTrue(array_equal(j.lineCont2.angle, j_expected.lineCont2.angle))
        self.assertTrue(array_equal(j.lineCont2.col, j_expected.lineCont2.col))
        self.assertTrue(array_equal(j.lineCont2.response, j_expected.lineCont2.response))
        self.assertTrue(array_equal(j.lineCont2.row, j_expected.lineCont2.row))
        self.assertTrue(j.lineCont2.father_frame == j_expected.lineCont2.father_frame)
        self.assertTrue(j.lineCont2.frame == j_expected.lineCont2.frame)
        self.assertTrue(array_equal(j.lineCont2.width_r, j_expected.lineCont2.width_r))
        self.assertTrue(array_equal(j.lineCont2.width_l, j_expected.lineCont2.width_l))
        self.assertTrue(array_equal(j.lineCont2.intensity, j_expected.lineCont2.intensity))

        #self.assertEqual(cont.cont_class, cont_expected.cont_class)
        self.assertEqual(cont.num, cont_expected.num)
        self.assertTrue(array_equal(cont.angle, cont_expected.angle))
        self.assertTrue(array_equal(cont.col, cont_expected.col))
        self.assertTrue(array_equal(cont.response, cont_expected.response))
        self.assertTrue(array_equal(cont.row, cont_expected.row))
        self.assertTrue(cont.father_frame == cont_expected.father_frame)
        self.assertTrue(cont.frame == cont_expected.frame)
        self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
        self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
        self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))


class Test_fillArray(unittest.TestCase):
    pass



class Test_getInterceptPoint(unittest.TestCase):
    def test_getInterceptPoint(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_line"), 'rb') as handle:
            line = pickle_load(handle)[0]
        self.assertTrue(array_equal(slopeOverlapResolver.getInterceptPoint(p1 = [23, 3], query=line), [47.21869366208475, 23.810754994126814]))


class Test_findLongestPath(unittest.TestCase):
    def test_findLongestPath(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_line"), 'rb') as handle:
            line = pickle_load(handle)[0]
        a = slopeOverlapResolver.findLongestPath(query=line, distance=3, points=[[1, 0], [3, 43], [2, 32], [1, 1], [9, 19]])
        self.assertTrue(array_equal(slopeOverlapResolver.findLongestPath(query=line, distance=3, points=[[1, 0], [3, 43], [2, 32], [1, 1], [9, 19]]), [47.21869366208475, 23.810754994126814]))




class Test_intersectsLine(unittest.TestCase):
    def test_intersectsLine(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_line"), 'rb') as handle:
            line = pickle_load(handle)[0]
        self.assertTrue(slopeOverlapResolver.intersectsLine(line, line, 1))



class Test_intersectsStart(unittest.TestCase):
    def test_intersectsStart(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_line"), 'rb') as handle:
            line = pickle_load(handle)[0]
        self.assertTrue(slopeOverlapResolver.intersectsStart(target=line, query=line, threshold=1))



class Test_intersectsEnd(unittest.TestCase):
    def test_intersectsEnd(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_line"), 'rb') as handle:
            line = pickle_load(handle)[0]
        self.assertTrue(slopeOverlapResolver.intersectsEnd(target=line, query=line, threshold=1))



class Test_intersects(unittest.TestCase):
    def test_intersects(self):
        self.assertFalse(slopeOverlapResolver.intersects(tEnd=[2, 9], qEnd=[10, 4], threshold=1))



class Test_getKey(unittest.TestCase):
    def test_getKey(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_junc"), 'rb') as handle:
            jun = pickle_load(handle)[0]
        a=slopeOverlapResolver.getKey(junction=jun)
        self.assertEqual(slopeOverlapResolver.getKey(junction=jun), '1447.9598039720820425.099282995828585')




class Test_getPoint(unittest.TestCase):
    def test_getPoint(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_line"), 'rb') as handle:
            line = pickle_load(handle)[0]
        self.assertTrue(array_equal(slopeOverlapResolver.getPoint(target=line, i=2), [47.21869366208475, 23.810754994126814]))



class Test_matchesStart(unittest.TestCase):
    def test_matchesStart(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_line"), 'rb') as handle:
            line = pickle_load(handle)[0]
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_junc"), 'rb') as handle:
            jun = pickle_load(handle)[0]
        self.assertFalse(slopeOverlapResolver.matchesEnd(junction=jun, line=line))



class Test_matchesEnd(unittest.TestCase):
    def test_matchesEnd(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_line"), 'rb') as handle:
            line = pickle_load(handle)[0]
        with open(path.join(ABSOLUTE_PATH, "pickle_files/valid_junc"), 'rb') as handle:
            jun = pickle_load(handle)[0]
        self.assertFalse(slopeOverlapResolver.matchesEnd(junction=jun, line=line))

if __name__ == '__main__':
    unittest.main()
