import unittest
from ridge_detection import width
from os import path
from numpy import array_equal
from pickle import load as pickle_load

ABSOLUTE_PATH = path.dirname(path.realpath(__file__))

class Test_constant_values(unittest.TestCase):
    def test_constants(self):
        self.assertEqual(width.LINE_WIDTH_COMPENSATION, 1.05)
        self.assertEqual(width.MIN_LINE_WIDTH, 0.1)
        self.assertEqual(width.MAX_CONTRAST,275.0)



class Test_bresenham(unittest.TestCase):
    def test_bresenham(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_bresenham"), 'rb') as handle:
            nx,  ny,  px,  py,  length,  line  = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_bresenham"), 'rb') as handle:
            num_line_expected, line_expected = pickle_load(handle)
        out=width.bresenham(nx=nx,  ny=ny,  px=px,  py=py,  length=length,  line=line  )
        self.assertEqual(num_line_expected, out)
        self.assertEqual(len(line_expected),len(line))
        for i,j in zip(line,line_expected):
            self.assertEqual(i.x,j.x)
            self.assertEqual(i.y, j.y)



class Test_fill_gaps(unittest.TestCase):
    def test_fill_gaps(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_fill_gaps"), 'rb') as handle:
            master, slave1, slave2, cont  = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_fill_gaps"), 'rb') as handle:
            master_expected , slave1_expected , slave2_expected , cont_expected = pickle_load(handle)

        width.fill_gaps(master=master, slave1=slave1,slave2=slave2, cont=cont)

        self.assertTrue(array_equal(master,master_expected))
        self.assertTrue(array_equal(slave1, slave1_expected))
        self.assertTrue(array_equal(slave2, slave2_expected))

        self.assertEqual(cont.cont_class,cont_expected.cont_class)
        self.assertEqual(cont.num, cont_expected.num)
        self.assertTrue(array_equal(cont.angle, cont_expected.angle))
        self.assertTrue(array_equal(cont.col, cont_expected.col))
        self.assertTrue(array_equal(cont.response, cont_expected.response))
        self.assertTrue(array_equal(cont.row, cont_expected.row))
        self.assertTrue(cont.father_frame== cont_expected.father_frame)
        self.assertTrue(cont.frame == cont_expected.frame)
        self.assertTrue(cont.intensity == cont_expected.intensity)
        self.assertTrue(cont.width_l == cont_expected.width_l)
        self.assertTrue(cont.width_r == cont_expected.width_r)



class Test_fix_locations(unittest.TestCase):
    def test_fill_gaps(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_fix_locations"), 'rb') as handle:
            width_l, width_r, grad_l, grad_r, pos_x, pos_y, correct, contrast, asymm, sigma, mode, correct_pos, cont = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_fix_locations"), 'rb') as handle:
            width_l_expected , width_r_expected , grad_l_expected , grad_r_expected , pos_x_expected , pos_y_expected , correct_expected , contrast_expected , asymm_expected , sigma_expected , mode_expected , correct_pos_expected , cont_expected = pickle_load(handle)

        width.fix_locations( width_l=width_l,  width_r=width_r,  grad_l=grad_l,  grad_r=grad_r,  pos_x=pos_x, pos_y=pos_y,  correction=correct,  contr=contrast,  asymm=asymm,  sigma=sigma,  mode=mode, correct_pos=correct_pos, cont=cont)

        self.assertTrue(array_equal(width_l, width_l_expected))
        self.assertTrue(array_equal(width_r, width_r_expected))
        self.assertTrue(array_equal(grad_l, grad_l_expected))
        self.assertTrue(array_equal(grad_r, grad_r_expected))
        self.assertTrue(array_equal(pos_x, pos_x_expected))
        self.assertTrue(array_equal(pos_y, pos_y_expected))
        self.assertTrue(array_equal(correct, correct_expected))
        self.assertTrue(array_equal(asymm, asymm_expected))
        self.assertTrue(array_equal(contrast, contrast_expected))

        self.assertEqual(sigma,sigma_expected)
        self.assertEqual(mode,mode_expected)
        self.assertEqual(sigma, sigma_expected)

        self.assertTrue(correct_pos == correct_pos_expected)

        self.assertEqual(cont.cont_class,cont_expected.cont_class)
        self.assertEqual(cont.num, cont_expected.num)
        self.assertTrue(array_equal(cont.angle, cont_expected.angle))
        self.assertTrue(array_equal(cont.col, cont_expected.col))
        self.assertTrue(array_equal(cont.response, cont_expected.response))
        self.assertTrue(array_equal(cont.row, cont_expected.row))
        self.assertTrue(cont.father_frame== cont_expected.father_frame)
        self.assertTrue(cont.frame == cont_expected.frame)
        self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
        self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
        self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))




class Test_compute_line_width(unittest.TestCase):
    def test_fill_gaps(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_compute_line_width"), 'rb') as handle:
            dx, dy, wwidth, height, sigma, mode, correct_pos, contours = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_compute_line_width"), 'rb') as handle:
            dx_expected , dy_expected , width_expected , height_expected , sigma_expected , mode_expected , correct_pos_expected , contours_expected = pickle_load(handle)
        width.compute_line_width(dx=dx, dy=dy, width=wwidth, height=height, sigma=sigma, mode=mode, correct_pos=correct_pos, contours=contours)

        self.assertTrue(array_equal(dx, dx_expected))
        self.assertTrue(array_equal(dy, dy_expected))
        self.assertTrue(array_equal(dx, dx_expected))

        self.assertEqual(wwidth, width_expected)
        self.assertEqual(height, height_expected)
        self.assertEqual(mode, mode_expected)
        self.assertEqual(sigma, sigma_expected)

        self.assertTrue(correct_pos==correct_pos_expected)

        self.assertEqual(len(contours),len(contours_expected))
        for cont,cont_expected in zip(contours,contours_expected):
            self.assertEqual(cont.cont_class,cont_expected.cont_class)
            self.assertEqual(cont.num, cont_expected.num)
            self.assertTrue(array_equal(cont.angle, cont_expected.angle))
            self.assertTrue(array_equal(cont.col, cont_expected.col))
            self.assertTrue(array_equal(cont.response, cont_expected.response))
            self.assertTrue(array_equal(cont.row, cont_expected.row))
            self.assertTrue(cont.father_frame== cont_expected.father_frame)
            self.assertTrue(cont.frame == cont_expected.frame)
            self.assertTrue(array_equal(cont.width_r, cont_expected.width_r))
            self.assertTrue(array_equal(cont.width_l, cont_expected.width_l))
            self.assertTrue(array_equal(cont.intensity, cont_expected.intensity))


if __name__ == '__main__':
    unittest.main()
