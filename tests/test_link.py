import unittest
from ridge_detection import link
from numpy import array_equal
from pickle import load as pickle_load
from os import path

ABSOLUTE_PATH = path.dirname(path.realpath(__file__))


class Test_constant_values(unittest.TestCase):
    def test_constants(self):
        self.assertEqual(link.MAX_ANGLE_DIFFERENCE, 0.5235987755982988)
        self.assertEqual(link.DOUBLE_MAX_VALUE_JAVA, 1.7976931348623157E308)
        self.assertTrue(array_equal(link.dirtab, [[[1, 0], [1, -1], [1, 1]], [[1, 1], [1, 0], [0, 1]],
                                                  [ [ 0, 1 ], [ 1, 1 ], [ -1, 1 ] ], [ [ -1, 1 ], [ 0, 1 ], [ -1, 0 ] ], [ [ -1, 0 ], [ -1, 1 ], [ -1, -1 ] ],
                                                  [ [ -1, -1 ], [ -1, 0 ], [ 0, -1 ] ], [ [ 0, -1 ], [ -1, -1 ], [ 1, -1 ] ], [ [ 1, -1 ], [ 0, -1 ], [ 1, 0 ] ]]))
        self.assertEqual(link.cleartab, [[[0, 1], [0, -1]], [[-1, 1], [1, -1]],
                                         [ [ -1, 0 ], [ 1, 0 ] ], [ [ -1, -1 ], [ 1, 1 ] ], [ [ 0, -1 ], [ 0, 1 ] ], [ [ 1, -1 ], [ -1, 1 ] ],
                                         [ [ 1, 0 ], [ -1, 0 ] ], [ [ 1, 1 ], [ -1, -1 ] ]])



class Test_Crossref_class(unittest.TestCase):
    c= link.Crossref(value=100)

    def test_same_value(self):
        self.assertEqual(self.c.compareTo(link.Crossref(value=100)), 0)

    def test_bigger_value(self):
        self.assertEqual(self.c.compareTo(link.Crossref(value=10)), -1)

    def test_smaller_value(self):
        self.assertEqual(self.c.compareTo(link.Crossref(value=1000)), 1)



class Test_closest_point(unittest.TestCase):
    def test_closest_point(self):
        a= link.closest_point(lx=100, ly=100, dx=20, dy=2, px=1, py=1)
        self.assertEqual(a[0], -7.821782178217816)
        self.assertEqual(a[1], 89.21782178217822)




class Test_interpolate_gradient(unittest.TestCase):
    def test_interpolate_gradient(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_interpolate_gradient"), 'rb') as handle:
            gradx, grady, nextpx, nextpy, width = pickle_load(handle)
        gx,gy= link.interpolate_gradient(gradx=gradx, grady=grady, px=nextpx, py=nextpy, width=width)
        self.assertEqual(gx,0.4970469180973688)
        self.assertEqual(gy, -6.296588807409601)




class Test_interpolate_response(unittest.TestCase):
    def test_interpolate_response(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_interpolate_response"), 'rb') as handle:
            resp, x, y, px, py, width, height = pickle_load(handle)
        self.assertEqual(link.interpolate_response(resp=resp, x=x, y=y, px=px, py=py, width=width, height=height), 6.696121834157805)





class Test_compute_contours(unittest.TestCase):
    def test_interpolate_response(self):
        with open(path.join(ABSOLUTE_PATH, "pickle_files/input_compute_contours"), 'rb') as handle:
            ismax, ev, n1, n2, p1, p2, k0, k1, contours, sigma, extend_lines, mode,  width, height, junctions = pickle_load(handle)
        with open(path.join(ABSOLUTE_PATH, "pickle_files/output_compute_contours"), 'rb') as handle:
            ismax_expected_out, ev_expected_out, n1_expected_out, n2_expected_out, p1_expected_out, p2_expected_out, k0_expected_out, k1_expected_out, contours_expected_out, sigma_expected_out, extend_lines_expected_out, mode_expected_out, width_expected_out, height_expected_out, junctions_expected_out,num_result_expected_out = pickle_load(handle)

        num_result = link.compute_contours(ismax=ismax, eigval=ev, normx=n1, normy=n2, posx=p1, posy=p2, gradx=k0, grady=k1, contours=contours, sigma=sigma, extend_lines=extend_lines, mode=mode, width=width, height=height, junctions=junctions)
        self.assertTrue(array_equal(ismax,ismax_expected_out))
        self.assertTrue(array_equal(ev,ev_expected_out))
        self.assertTrue(array_equal(n1,n1_expected_out))
        self.assertTrue(array_equal(n2,n2_expected_out))
        self.assertTrue(array_equal(p1,p1_expected_out))
        self.assertTrue(array_equal(p2,p2_expected_out))
        self.assertTrue(array_equal(k0,k0_expected_out))
        self.assertTrue(array_equal(k1,k1_expected_out))

        self.assertEqual(len(contours),len(contours_expected_out))
        for line_i,line_j in zip(contours,contours_expected_out):
            #self.assertEqual(line_i._id,line_j._id)        # since it is based on a global counter variable, when i run 'test_all.py' it fails
            self.assertEqual(line_i.num, line_j.num)
            if line_i.angle is not None:
                self.assertTrue(array_equal(line_i.angle,line_j.angle))
            if line_i.asymmetry is not None:
                self.assertTrue(array_equal(line_i.asymmetry, line_j.asymmetry))
            if line_i.col is not None:
                self.assertTrue(array_equal(line_i.col, line_j.col))
            if line_i.response is not None:
                self.assertTrue(array_equal(line_i.response, line_j.response))
            if line_i.row is not None:
                self.assertTrue(array_equal(line_i.row, line_j.row))
            if line_i.intensity is not None:
                self.assertTrue(array_equal(line_i.intensity, line_j.intensity))
            if line_i.width_l is not None:
                self.assertTrue(array_equal(line_i.width_l, line_j.width_l))
            if line_i.width_r is not None:
                self.assertTrue(array_equal(line_i.width_r, line_j.width_r))
            self.assertEqual(line_i.cont_class,line_j.cont_class)
            if line_i.father_frame is not None:
                self.assertTrue(array_equal(line_i.father_frame, line_j.father_frame))
            if line_i.frame is not None:
                self.assertTrue(array_equal(line_i.frame , line_j.frame ))



        self.assertEqual(sigma,sigma_expected_out)
        self.assertTrue(extend_lines==extend_lines_expected_out)
        self.assertEqual(mode,mode_expected_out)
        self.assertEqual(num_result, num_result_expected_out)
        self.assertEqual(height, height_expected_out)
        self.assertEqual(width, width_expected_out)

        self.assertEqual(len(junctions),len(junctions_expected_out))
        for j1,j2 in zip(junctions,junctions_expected_out):
            self.assertEqual(j1.cont1,j2.cont1)
            self.assertEqual(j1.cont2, j2.cont2)
            self.assertEqual(j1.pos, j2.pos)
            self.assertEqual(j1.y, j2.y)
            self.assertEqual(j1.x, j2.x)
            if j1.father_frame is not None:
                self.assertTrue(array_equal(j1.father_frame, j2.father_frame))
            self.assertTrue(j1.isNonTerminal==j2.isNonTerminal)
            # I skip the lineCont because they are None

if __name__ == '__main__':
    unittest.main()
